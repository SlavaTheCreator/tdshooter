// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "../Game/TDShooterGameInstance.h"
// #include <FunctionalTestUtilityLibrary.h>

ATDShooterCharacter::ATDShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	InitWeapon(InitWeaponName);

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void ATDShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	MovementTick(DeltaSeconds);
	UpdateEnergy(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
}

void ATDShooterCharacter::SetupPlayerInputComponent(class UInputComponent* NewInputComponent){
    Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDShooterCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDShooterCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDShooterCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDShooterCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDShooterCharacter::TryRealoadWeapon);
}

void ATDShooterCharacter::InputAxisX(float Value){
	AxisX = Value;
}
void ATDShooterCharacter::InputAxisY(float Value){
	AxisY = Value;
}

void ATDShooterCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATDShooterCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATDShooterCharacter::MovementTick(float DeltaTime){
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	//��������� ��������� � ����������� �� ����������� ��������
	if(isRunning)
		GetCharacterMovement()->MaxWalkSpeed = isForwardMove() ? MovementInfo.RunSpeed : MovementInfo.WalkSpeed;

	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if(myController){
		FHitResult ResultHit;
		//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
		myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

		float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));

		if (CurrentWeapon) {
			FVector Displacement = FVector(0);
			switch (MovementState)
			{
			case EMovementState::Aim_State:
				Displacement = FVector(0.0f, 0.0f, 160.0f);
				break;
			case EMovementState::Walk_State:
				Displacement = FVector(0.0f, 0.0f, 120.0f);
				break;
			case EMovementState::Run_State:
				Displacement = FVector(0.0f, 0.0f, 120.0f);
				break;
			case EMovementState::Sit_State:
				Displacement = FVector(0.0f, 0.0f, 80.0f);
				break;
			default:
				break;
			}

			CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
		}
	}
}

void ATDShooterCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void ATDShooterCharacter::CharacterUpdate()
{
	float ResSpeed = 400.0f;
	bool _isAim = false;
	isRunning = false;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		_isAim = true;
		break;
	case EMovementState::Run_State:
		isRunning = true;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	default:
		break;
	}
	if (this->isSit) {
		ResSpeed = MovementInfo.SitSpeed;
	} 

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
	this->isAim = _isAim;
}

void ATDShooterCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

void ATDShooterCharacter::UpdateEnergy(float DeltaSeconds)
{
	if (isRunning && isForwardMove()) {
		SubtractEnergy(DeltaSeconds);
	}
	else {
		AddEnergy(DeltaSeconds);
	}
}

void ATDShooterCharacter::SubtractEnergy(float val) {
	Energy -= val;
	if (Energy <= 0) {
		Energy = 0;
		ChangeMovementState(EMovementState::Walk_State);
	}
}

void ATDShooterCharacter::AddEnergy(float val) {
	Energy += val;
	if (Energy > MaxEnergy) {
		Energy = MaxEnergy;
	}
}

bool ATDShooterCharacter::isForwardMove() {
	FVector fVector = GetActorForwardVector();
	FVector lmVector = GetLastMovementInputVector();
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("%f = FloatVariable"), FVector::DotProduct(fVector, lmVector)));
	return FVector::DotProduct(fVector, lmVector) > 0.9f;
}

UDecalComponent* ATDShooterCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

AWeaponDefault* ATDShooterCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDShooterCharacter::InitWeapon(FName IdWeapon)
{
	UTDShooterGameInstance* myGI = Cast<UTDShooterGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI) {
		if (myGI->GetWeaponInfoByName(IdWeapon, myWeaponInfo)) {
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("hand_r_ability_socket"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDShooterCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDShooterCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFire.AddDynamic(this, &ATDShooterCharacter::WeaponFire);
				}
			}
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("ATDShooterCharacter::InitWeapon - Weapon not found in table"));
		}
	}
}

void ATDShooterCharacter::TryRealoadWeapon() {
	if (CurrentWeapon) {
		if (CurrentWeapon->GetWeaponRound() <= CurrentWeapon->WeaponSetting.MaxRound) {
			CurrentWeapon->InitReload();
		}
	}
}

void ATDShooterCharacter::WeaponReloadStart(UAnimMontage* Anim) {
	WeaponReloadStart_BP(Anim);
}

void ATDShooterCharacter::WeaponReloadEnd() {
	WeaponReloadEnd_BP();
}

void ATDShooterCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

void ATDShooterCharacter::WeaponReloadEnd_BP_Implementation()
{
	//in BP
}

void ATDShooterCharacter::WeaponFire(UAnimMontage* Anim)
{
	//->GetAnimInstance()->Montage_Play(WeaponSetting.AnimWeaponInfo.AnimWeaponFire);
	this->GetMesh()->GetAnimInstance()->Montage_Play(Anim);
}